package cz.cvut.fel.omo.cv8;

import com.google.common.collect.ImmutableList;
import java.util.HashMap;
import java.util.Map;

public class Context {
    private Map<String, ImmutableList<Integer>> map = new HashMap<>();

    public void put(String name, ImmutableList<Integer> value) {
        map.put(name, ImmutableList.copyOf(value));
    }

    public ImmutableList<Integer> get(String name) {
        return ImmutableList.copyOf(map.get(name));
    }
}
