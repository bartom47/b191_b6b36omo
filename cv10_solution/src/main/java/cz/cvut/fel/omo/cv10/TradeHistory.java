package cz.cvut.fel.omo.cv10;

import java.util.*;
import java.util.stream.Collectors;

import static java.util.Comparator.comparing;
import static java.util.stream.Collectors.*;

public class TradeHistory {

    public List<Transaction> transactions;

    public TradeHistory(List<Transaction> transctions) {
        this.transactions = transctions;
    }

    public List<Transaction> findAllTransactionsIn2011AndSortByValueAsc(){
        List<Transaction> newList = transactions.stream()
                  .filter(Transaction::getYear == 2011)
                  .sorted(Comparator.comparing(Transaction::getValue))
                  .collect(Collectors.toList());
        return newList;
    }

    public List<String> getUniqueCitiesSortedAsc(){
        List<String> newList = transactions.stream()
                               .map(Transaction::getTrader)
                               .map(Trader::getCity)
                               .distinct()
                               .sorted()
                               .collect(toList());
        return newList;
    }

    /*
    * String shall start with "Traders:" and use space as separator. E.g.: "Traders: Bob George"
    *
     */
    public String getSingleStringFromUniqueTradersNamesSortByNameAsc(){
        String traderStr = transactions.stream()
                            .map(Transaction::getTrader)
                            .map(Trader::getName)
                            .distinct()
                            .sorted()
                            .reduce("Traders:",(str1, str2) -> str1.concat(" "+str2));
        return traderStr;
    }

    public boolean isSomeTraderFromCity(String cityName){
        boolean isSome = transactions.stream().map(Transaction::getTrader).anyMatch(t -> t.getCity().equals(cityName));
        return isSome;
    }

    public Optional<Transaction> findSmallestTransactionUsingReduce(){
        Optional<Transaction> smallestTransaction = transactions.stream().reduce((a,b) -> a.getValue()<b.getValue()?a:b);

        return smallestTransaction;
    }

    public Map<String, List<Trader>> getTradersByTown(){
        Map<String, List<Trader>> tradersByTown = transactions.stream().map(a->a.getTrader()).distinct().collect(Collectors.groupingBy(a->a.getCity()));
        return tradersByTown;
    }

    public Map<String, Long> getTradersCountsByTown(){
        Map<String, Long> tradersByTown = transactions.stream().map(a->a.getTrader()).distinct()
                .collect(Collectors.groupingBy(a->a.getCity(),Collectors.counting()));
        return tradersByTown;
    }

    public Map<Boolean, List<Transaction>> partitionTransactionsByTraderIsVegetarian(){
        Map<Boolean, List<Transaction>> transactionsBy = transactions.stream().collect(partitioningBy(a->a.getTrader().isVegetarian()));
        return transactionsBy;
    }
}
