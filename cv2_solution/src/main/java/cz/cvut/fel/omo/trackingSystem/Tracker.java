package cz.cvut.fel.omo.trackingSystem;

/**
 * Created by kuki on 22/09/2017.
 * Tracker is device installed into company vehicles, connected to car computer in order to obtain necessary data.
 */
public class Tracker {

    private final int trackerID;
    private Vehicle currentVehicle;
    private int innerMemory;

    public Tracker(int trackerID){
        this.trackerID = trackerID;
    }

    public int getTrackerMileage(){
        return currentVehicle.getMileage() - innerMemory;
    }

    public void attachTracker(Vehicle vehicle){
        currentVehicle = vehicle;
        innerMemory = vehicle.getMileage();
    }

    public void resetTrackerMileage(){
        this.innerMemory = currentVehicle.getMileage();
    }

    public Vehicle getCurrentVehicle() {
        return currentVehicle;
    }

    public String toString(){
        return getClass().getSimpleName() + "_" + trackerID + ", attached to " + currentVehicle.toString() + "\n";
    }
}
