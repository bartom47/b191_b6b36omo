package cz.cvut.fel.omo.cv7.ab;

import cz.cvut.fel.omo.cv7.Loan;
import org.javamoney.moneta.Money;

import javax.money.MonetaryAmount;

/** Kurz A7B36OMO - Objektove modelovani - Cviceni 7 Abstract factory, factory method, singleton, dependency injection
 *
 *  @author mayerto1
 *
 *
 */
public class AbLoan implements Loan{

    MonetaryAmount balance;
    double interestRate;
    int repaymentPeriod;

    public AbLoan(MonetaryAmount amount, int months, double recommendedInterestRate){
        balance = amount;
        interestRate = recommendedInterestRate;
        repaymentPeriod = months;
    }

    @Override
    public MonetaryAmount getBalance() {
        return balance;
    }

    @Override
    public double getInterestRate() {
        return interestRate;
    }

    @Override
    public MonetaryAmount getMonthlyPayment() {
        return balance=balance.divide(repaymentPeriod).add(balance.multiply(interestRate/12)).add(Money.of(10,"EUR"));
    }

    public String toString(){
        return String.format("Ab Bank Loan Overview - Balance: %s, InterestRate: %f, MonthlyPayment: %s", getBalance(), getInterestRate(), getMonthlyPayment());
    }
}
