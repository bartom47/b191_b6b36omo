package cz.cvut.fel.omo.cv7.ab;

import cz.cvut.fel.omo.cv7.BankOffice;

/** Kurz A7B36OMO - Objektove modelovani - Cviceni 7 Abstract factory, factory method, singleton, dependency injection
 *
 *  @author mayerto1
 *
 *
 */
public class AbBankOffice implements BankOffice {

    String ADDRESS = "Ab, Namesti 2, Praha 1";
    String PHONE = "420-2-777-777-777";

    @Override
    public String getAddress() {
        return ADDRESS;
    }

    @Override
    public String getPhoneContact() {
        return PHONE;
    }

    public String toString(){
        return String.format("Cen" + "tral Office Address: %s, Phone Number: %s",getAddress(),getPhoneContact());
    }
}
