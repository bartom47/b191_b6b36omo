package cz.cvut.fel.omo.blog;



/**
 * @author Matej
 * @version 1.0
 * @created 13-��j-2019 17:07:34
 */
public interface DashboardElement {

    public void display();

}