/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fel.omo;

import java.util.ArrayList;
import java.util.EmptyStackException;
import java.util.Hashtable;
import java.util.NoSuchElementException;

/**
 *
 * @author Matej
 */
public class ExtendedStackImpl implements ExtendedStack{
    private Stack stack;
    
    public ExtendedStackImpl() {
        stack = generateStack();
    }
    
    private Stack generateStack(){
        return new StackImpl();
    }
    
    @Override
    public void push(int toInsert) {
        stack.push(toInsert);
    }

    @Override
    public void push(int[] toInsert) {
        for(int toInsertSingle : toInsert){
            push(toInsertSingle);
        }
    }

    @Override
    public int top() {
        if (stack.isEmpty()) {
            throw new EmptyStackException();
        }
        Hashtable<Integer, Integer> stackElements = new Hashtable();
        int toPop = 0;
        int stackSize = stack.getSize();

        Stack newStack = generateStack();

        for (int i = stack.getSize() - 1; i >= 0; --i) {
            stackElements.put(i, stack.pop());
        }
        for (int i = 0; i < stackSize; i++) {
            Integer element = stackElements.get(i);
            if (i == 0) {
                toPop = element;
                stackElements.remove(toPop);
            }
            newStack.push(toPop);
        }
        stack = newStack;        
        return toPop;
        
            
    }

    @Override
    public int pop() {
        return stack.pop();
    }

    @Override
    public int popFirstNegativeElement() {
        if(stack.isEmpty()){
            throw new EmptyStackException();
        }
        Hashtable<Integer, Integer> stackElements = new Hashtable();        
        int toPop = 0;
        int stackSize = stack.getSize();
        
        Stack newStack = generateStack();
                
        for(int i = stack.getSize() - 1; i >= 0; --i){
            stackElements.put(i, stack.pop());
        }
        for(int i = 0; i < stackSize; i++){
            Integer element = stackElements.get(i);
            if(element < 0){
                toPop = element;
                stackElements.remove(toPop);                
            } else {
                newStack.push(element);
            }            
        }
        stack = newStack;
        if(toPop == 0){
            throw new NoSuchElementException("No negative element found in the stack.");
        }
        return toPop;
    }       

    @Override
    public boolean isEmpty() {
        return stack.isEmpty();
    }

    @Override
    public int getSize() {
        return stack.getSize();
    }
    
}
