package cz.cvut.fel.omo.cv6.strategy;

import cz.cvut.fel.omo.cv6.Street;
import cz.cvut.fel.omo.cv6.TrafficLight;

/** Kurz A7B36OMO - Objektove modelovani - Cviceni 6 Design Patterns State, strategy
 *
 *  @author mayerto1
 *
 *  The strategy used for traffic control during evening hours.
 */
public class EveningStrategy extends Strategy {

    int time = 0;

    public EveningStrategy(Street street) {
        super(street);
    }

    @Override
    public void controlTraffic() {

        int counter = street.getLights().size()-1;

        for (TrafficLight light : street.getLights()) {
 
            if ( time - counter * lightDistance == 0)  {
                light.startGoSequence();
            } else {
                light.timeLapseTick();
            }
            counter--;
        }
        time++;

    }
}
