package cz.cvut.fel.omo.server;

import cz.cvut.fel.omo.Observable;
import cz.cvut.fel.omo.Observer;

import javax.money.MonetaryAmount;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public abstract class CryptoCurrencyController implements Observable {

    private final List<Observer> observers = new ArrayList();

    protected CryptoCurrency currency;
    int PERCENTAGE = 100;

    protected CryptoCurrencyController(CryptoCurrency currency) {
        this.currency = currency;
    }
    
    /*
     * Method for calculating the new price of cryptocurrency.
     */
    public void changePrice(int fluctuation) {
        MonetaryAmount currentPrice = currency.getPrice();
        MonetaryAmount change = currentPrice.multiply(fluctuation).divide(PERCENTAGE);
        currency.setPrice(currentPrice.add(change));
        currency.printMessage();
        notifyAllObservers();        
    }

    public MonetaryAmount getState(){
        return currency.getPrice();
    }

    @Override
    public void attach(Observer observer) {
        Objects.requireNonNull(observer);
        observers.add(observer);
    }

    @Override
    public void detach(Observer observer) {
        Objects.requireNonNull(observer);
        observers.remove(observer);
    }

    @Override
    public void notifyAllObservers() {
        observers.stream().forEach(observer -> observer.update());
    }  
    
}
