package cz.cvut.fel.omo.client;

import cz.cvut.fel.omo.Observer;
import cz.cvut.fel.omo.server.Bitcoin;

public class BitcoinClient implements Observer{

    StockExchangeClient context;

    public BitcoinClient(StockExchangeClient context) {
        this.context = context;
    }

    // IMPLEMENT ME
        // This class should implement observer interface.
        // On update the class should print name of context class and current price of bitcoin.

    @Override
    public void update() {
        final String contextName = context.getName();
        final String bitcoinPrice = context.getServer().getBitcoinState().toString();        
        
        System.out.println(String.format(StockExchangeClient.PRINT_TEMPLATE, contextName, Bitcoin.class.getSimpleName(), bitcoinPrice));
    }
    
    
}
