package cz.cvut.fel.omo.client;

import cz.cvut.fel.omo.Observer;
import cz.cvut.fel.omo.server.StockExchangeServer;

public class StockExchangeClient {
    
    static final String PRINT_TEMPLATE = "\tContext name: %s\r\n\t\t%s price: %s";

    private StockExchangeServer stockExchangeServer;
    private String name;
    
    private BitcoinClient bitcoinClient;
    private LitecoinClient litecoinClient;

    public StockExchangeClient(StockExchangeServer stockExchange, String name){
        this.stockExchangeServer = stockExchange;
        this.name = name;
        bitcoinClient = new BitcoinClient(this);
        litecoinClient = new LitecoinClient(this);
    }

    /*
     * Method makes sure, that this client application is subscribed to the bitcoin channel.
     */
    public void subscribeToBitcoinChannel(){
        stockExchangeServer.subscribeToBitcoinUpdates(bitcoinClient);
    }

    /*
     * Method makes sure, that this client application is subscribed to the litecoin channel.
     */
    public void subscribeToLitecoinChannel(){
        stockExchangeServer.subscribeToBitcoinUpdates(litecoinClient);
    }

    /*
     * Method makes sure, that this client application is subscribed to both litecoin and bitcoin channel.
     */
    public void subscribeToAllChannels(){
        subscribeToBitcoinChannel();
        subscribeToLitecoinChannel();
    }

    /*
     * Method unsubscribes application from bitcoin channel, hereafter no notifications about bitcoin price will be delivered.
     */
    public void unsubscribeFromBitcoinChannel(){
        stockExchangeServer.unsubscribeFromBitcoinChannel(bitcoinClient);
    }

    /*
     * Method unsubscribes application from litecoin channel, hereafter no notifications about litecoin price will be delivered.
     */
    public void unsubscribeFromLitecoinChannel(){
        stockExchangeServer.unsubscribeFromBitcoinChannel(litecoinClient);
    }

    /*
     * Method unsubscribes application from both bitcoin and litecoin channels, hereafter no notifications about bitcoin nor litecoin price will be delivered.
     */
    public void unsubscribeFromAllChannels(){
        unsubscribeFromBitcoinChannel();
        unsubscribeFromLitecoinChannel();
    }

    public String getName(){
        return name;
    }

    public StockExchangeServer getServer(){
        return stockExchangeServer;
    }
}
