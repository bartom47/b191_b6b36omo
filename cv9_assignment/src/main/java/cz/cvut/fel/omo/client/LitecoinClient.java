package cz.cvut.fel.omo.client;

import cz.cvut.fel.omo.Observer;

public class LitecoinClient implements Observer{

    StockExchangeClient context;

    public LitecoinClient(StockExchangeClient context){
        this.context = context;
    }

    // IMPLEMENT ME
        // This class should implement observer interface.
        // On update the class should print name of context class and current price of litecoin.

    @Override
    public void update() {
        final String contextName = context.getName();
        final String litecoinPrice = context.getServer().getLitecoinState().toString();

        System.out.println(String.format(StockExchangeClient.PRINT_TEMPLATE, contextName, "Litecoin", litecoinPrice));
    }
}
