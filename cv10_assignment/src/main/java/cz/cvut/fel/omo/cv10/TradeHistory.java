package cz.cvut.fel.omo.cv10;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.partitioningBy;

public class TradeHistory {

    public List<Transaction> transactions;

    public TradeHistory(List<Transaction> transctions) {
        this.transactions = transctions;
    }

    public List<Transaction> findAllTransactionsIn2011AndSortByValueAsc(){       
        //Implement body here
        List<Transaction> newList = transactions.stream()
                .filter(transaction -> transaction.getYear() == 2011)
                .sorted((t1, t2) -> Integer.compare(t1.getValue(), t2.getValue()))
                .collect(Collectors.toList());
        return newList;
    }

    public List<String> getUniqueCitiesSortedAsc(){        
        //Implement body here
        List<String> cities = transactions.stream()
                .map(transaction -> transaction.getTrader().getCity())
                .distinct()
                .sorted()
                .collect(Collectors.toList());
                
        return cities;
    }

    /*
    * String shall start with "Traders:" and use space as separator. E.g.: "Traders: Bob George"
    *
     */
    public String getSingleStringFromUniqueTradersNamesSortByNameAsc(){        
        StringBuilder traderStr = new StringBuilder("Traders:");
        //Implement body here
        transactions.stream()
                .map(transaction -> transaction.getTrader().getName())
                .distinct()
                .sorted()                
                .forEach((traderName) -> traderStr.append(" ").append(traderName));
        return traderStr.toString();
    }

    public boolean isSomeTraderFromCity(String cityName){        
        //Implement body here
        List<String> cities = this.getUniqueCitiesSortedAsc();
        return cities.contains(cityName);
    }

    public Optional<Transaction> findSmallestTransactionUsingReduce(){
        Optional<Transaction> smallestTransaction = transactions.stream()        
                .reduce((t1, t2) -> t1.getValue() < t2.getValue() ? t1 : t2);
        return smallestTransaction;
    }

    public Map<String, List<Trader>> getTradersByTown(){
        Map<String, List<Trader>> tradersByTown = transactions.stream()                
                .map(transaction -> transaction.getTrader())
                .distinct()
                .collect(Collectors.groupingBy(Trader::getCity));
                
        return tradersByTown;
    }

    public Map<String, Long> getTradersCountsByTown(){
        Map<String, Long> tradersByTown = transactions.stream()
                .map(transaction -> transaction.getTrader())
                .distinct()
                .collect(Collectors.groupingBy(o -> o.getCity(), Collectors.counting()));                               
        return tradersByTown;
    }

    public Map<Boolean, List<Transaction>> partitionTransactionsByTraderIsVegetarian(){
        Map<Boolean, List<Transaction>> transactionsBy = transactions.stream()
                .collect(Collectors.groupingBy(t -> t.getTrader().isVegetarian(), Collectors.toList()));
        return transactionsBy;
    }
}
