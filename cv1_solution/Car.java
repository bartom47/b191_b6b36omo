import java.util.ArrayList;
import java.util.List;

public class Car extends CarPart {
    private int makeYear;
    private int color;
    private List <Wheel> wheels = new ArrayList<>();


    public Car(int wheelsCount, int wheelRadius, int makeYear, int color) {
        this.makeYear = makeYear;
        this.color = color;
        for(int i=0; i<wheelsCount; i++){
            wheels.add(new Wheel(wheelRadius));
        }
    }

    public int getMakeYear() {

        return makeYear;
    }

    public int getColor() {

        return color;
    }

}
