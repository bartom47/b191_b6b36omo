/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fel.omo.cv7.ab;

import cz.cvut.fel.omo.cv7.AbstractBankFactory;
import cz.cvut.fel.omo.cv7.Account;
import cz.cvut.fel.omo.cv7.BankOffice;
import cz.cvut.fel.omo.cv7.Loan;
import javax.money.MonetaryAmount;

/**
 *
 * @author Matej
 */
public class AbBankFactory extends AbstractBankFactory{
    private static AbBankFactory INSTANCE;

    @Override
    public BankOffice createBankOffice() {
        return new AbBankOffice();
    }

    @Override
    public Account createAccount() {
        return new AbAccount();
    }

    @Override
    public Loan createLoan(MonetaryAmount amount, int months, double recommendedInterestRate) {
        return new AbLoan(amount, months, recommendedInterestRate);
    }
    
    public static synchronized AbBankFactory getInstance(){
        if(INSTANCE == null){
            INSTANCE = new AbBankFactory();
        }
        return INSTANCE;
    }
    
}
