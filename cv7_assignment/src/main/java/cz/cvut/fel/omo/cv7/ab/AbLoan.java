/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fel.omo.cv7.ab;

import cz.cvut.fel.omo.cv7.Loan;
import javax.money.MonetaryAmount;
import org.javamoney.moneta.Money;

/**
 *
 * @author Matej
 */
public class AbLoan implements Loan{
    private static final String CURRENCY = "EUR";
    
    private static final MonetaryAmount FIXED_FEE = Money.of(10, CURRENCY);
    
    private MonetaryAmount balance;
    private final double interestRate;
    private final int countMonths;

    public AbLoan(MonetaryAmount balance, int countMonths, double interestRate) {
        this.balance = balance;
        this.interestRate = interestRate;
        this.countMonths = countMonths;
    }   
    
    @Override
    public MonetaryAmount getBalance() {
        return balance;
    }

    @Override
    public double getInterestRate() {
        return interestRate;
    }

    @Override
    public MonetaryAmount getMonthlyPayment() {
        return balance.divide(countMonths).add(balance.multiply(interestRate / 12)).add(FIXED_FEE);
    }
    
}
