/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fel.omo.cv7.ab;

import cz.cvut.fel.omo.cv7.BankOffice;

/**
 *
 * @author Matej
 */
public class AbBankOffice implements BankOffice{
    private final String ADDRESS = "Ab Banka, Náměstí 2, Praha 1";
    private final String PHONE = "420-2-777-777-777";

    @Override
    public String getAddress() {
        return ADDRESS;
    }

    @Override
    public String getPhoneContact() {
        return PHONE;
    }
    
    @Override
    public String toString() {
        return String.format("Bank Address: %s, Phone Number: %s", getAddress(), getPhoneContact());
    }
}
