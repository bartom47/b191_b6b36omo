/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fel.omo.cv7.ab;

import cz.cvut.fel.omo.cv7.Account;
import javax.money.MonetaryAmount;
import org.javamoney.moneta.Money;

/**
 *
 * @author Matej
 */
public class AbAccount implements Account{
    private Long countWithdraw = 0L;
    private Long countDeposit = 0L;
    
    private static final String CURRENCY = "EUR";    
    
    private static final MonetaryAmount WITHDRAW_COEFFICIENT = Money.of(0.5, CURRENCY);
    private static final MonetaryAmount DEPOSIT_COEFFICIENT = Money.of(-0.25, CURRENCY);

    private static final MonetaryAmount WITHDRAW_LIMIT = Money.of(0, CURRENCY);
    
    private MonetaryAmount balance = Money.of(0, CURRENCY);

    @Override
    public MonetaryAmount getBalance() {
        return balance;
    }

    @Override
    public MonetaryAmount getWithdrawLimit() {
        return WITHDRAW_LIMIT;
    }

    @Override
    public MonetaryAmount getMonthlyFee() {        
        return WITHDRAW_COEFFICIENT.multiply(countWithdraw).add(DEPOSIT_COEFFICIENT.multiply(countDeposit));
    }

    @Override
    public void withdraw(MonetaryAmount amount) {
        countWithdraw++;
        balance = balance.subtract(amount);
    }

    @Override
    public void deposit(MonetaryAmount amount) {
        countDeposit++;
        balance = balance.add(amount);
    }

    @Override
    public String toString() {
        return String.format("Ab Account - balance: %s, fee: %s", getBalance(), getMonthlyFee());
    }
    
    
    
}
