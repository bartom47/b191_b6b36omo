package cz.cvut.fel.omo.blog;

import java.util.List;
import java.util.stream.Collectors;

public interface BlogInterface {

    /*
     *   Factory method for creating user accounts.
     *   @param username  Account name serving as a unique identifier across user accounts.
     *   @param password  Password, that will be used to log into blog
     *   @param admin     Flag deciding, whether newly created object ought to be standard or admin account.
    */
    public void createNewAccount(String username, String password, boolean admin);


    /*
     *  Method for blocking user account
     *  @param username  Username of account to be blocked
     */
    public void blockUserAccount(String username);

    /*
    * This method goes through list of users and searches for given username.
    * @param username  Name of the blogAccount to be found
    * @return boolean  Does the blogAccount exists?
     */
    public boolean existsUserAccount(String username);

    /*
    * This method simulates loginig into the blog. Method goes through list of user accounts searches for user
    * account.
    * @param username  Username of account to return.
    * @param password  Password for searched account.
    * @return UserAccount  UserAccount matching criteria or null.
    *
    */
    public BlogAccount login(String username, String password);

    /*
    *   This method allows admin to create new Topic for the blog.
    *   @param newTopic Unique name of topic to be submitted.
    */
    public void submitNewTopic(String topicName, String topicDescription);

    /*
     * Method goes through list of topics and searches for given topic.
     * @param topicName  Name of the topic to be found.
     * @return boolean  Does the topic exists?
     */
    public boolean existsTopic(String topicName);

    /*
     * Displaying of List of topics is delegated on to the Dashboard. Displaying one specific topic is them delegated on to Object Topic itself.
     */
    public void displayTopics();



    /*
     * Method for finding specific Topic according to topicName.
     * @param topicName  Name of searched topic
     */
    public Topic findTopic(String topicName);

    /*
     * Factory method for creating new Posts.
     */
    public void submitNewPost(String postHeader, String postContent, BlogAccount account);

    /*
     * Method for finding specific Post according to postHeader.
     * @param postHeader  Header of searched Post.
     */
    public Post findPost(String postHeader);

    /*
     * Method goes through list of posts and searches for given post.
     * @param postHeader  Header of post to be found.
     * @return boolean  Does the post exists?
     */
    public boolean existsPost(String postHeader);

    /*
    * Method for displaying posts of the blog.
    */
    public void readBlog();

    /*
    * Method for displaying posts of the blog under given topic.
    * @param Name of the topic to be displayed
    */
    public void readBlog(String topicName);

}
