package cz.cvut.fel.omo.blog;

import java.text.SimpleDateFormat;

public class Comment implements DisplayableComponent {

    private String commentContent;
    private BlogAccount author;

    public Comment(String commentContent, BlogAccount author) {
        this.commentContent = commentContent;
        this.author = author;
    }

    public void displayComponent() {
        System.out.print(commentContent + "\n");
        System.out.print(author);
    }

    public String toString(){
        return author + ": " + commentContent + "\n";
    }
}

