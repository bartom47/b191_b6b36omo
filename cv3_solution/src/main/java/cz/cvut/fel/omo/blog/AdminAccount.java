package cz.cvut.fel.omo.blog;

import java.util.Scanner;

public class AdminAccount extends BlogAccount{

    private Scanner scanner = new Scanner(System.in);

    public AdminAccount(String username, String password, Blog blog){
        super(username, password, blog);
    }

    private void submitNewPost(String postHeader, String postContent){
        blog.submitNewPost(postHeader, postContent, this);
    }

    private void submitNewTopic(String topicName, String topicDescription){
        blog.submitNewTopic(topicName, topicDescription);
    }

    /*
     * Non-interactive version of method for creating new post.
     * @param postHeader  Header of newly created post.
     * @param postContent  Content of newly created post.
     */
    public void writeNewPost(String postHeader, String postContent){
        submitNewPost(postHeader, postContent);
    }

    /*
     * Interactive version of method for creating new post.
     */
    public void writeNewPost(){
        System.out.println("Writing new post.\nEnter post header");
        String postHeader = scanner.next();
        System.out.println("Enter content of your post:");
        String postContent = scanner.next();
        submitNewPost(postHeader, postContent);
    }

    /*
     * Non-interactive version of method for creating new post.
     * @param topicName  Name of newly created topic, must be unique across all blog topics.
     * @param description  Short description of the topic.
     */
    public void createNewTopic(String topicName, String topicDescription){
        submitNewTopic(topicName, topicDescription);
    }

    /*
     * Interactive version of method for creating new topic.
     */
    public void createNewTopic() {
        System.out.println("Creating new topic. \nEnter topic name:");
        String topicName = scanner.next();
        System.out.print("\nEnter topic description:\n");
        String topicDescription = scanner.next();
        submitNewTopic(topicName, topicDescription);
    }

}
