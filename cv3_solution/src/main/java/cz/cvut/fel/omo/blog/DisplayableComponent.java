package cz.cvut.fel.omo.blog;

public interface DisplayableComponent {

    public void displayComponent();
}
