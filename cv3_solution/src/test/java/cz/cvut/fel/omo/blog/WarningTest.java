package cz.cvut.fel.omo.blog;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.Assert.*;

public class WarningTest {

    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();
    private Warning warning;

    @Before
    public void executeBeforeEach(){
        System.setOut(new PrintStream(outContent));
        System.setErr(new PrintStream(errContent));
        warning = new Warning("Login failed. Submitted wrong password or username");
    }

    @After
    public void cleanUpStreams() {
        System.setOut(null);
        System.setErr(null);
    }

    @Test
    public void displayComponent_displayWarning_warningDisplayedCorrectly() {
        // arrange
        String expectedString = "Login failed. Submitted wrong password or username";
        // act
        warning.displayComponent();
        // assert
        assertEquals(expectedString, outContent.toString());
    }
}