package cz.cvut.fel.omo.blog;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.junit.Assert.*;

public class PostTest {

    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();
    private Post post;

    @Before
    public void setUpStreams() {
        System.setOut(new PrintStream(outContent));
        System.setErr(new PrintStream(errContent));
        post = new Post("AngularJS", "Cachy content", new AdminAccount("author", "password", null));
    }

    @After
    public void cleanUpStreams() {
        System.setOut(null);
        System.setErr(null);
    }

    @Test
    public void displayComponent_displayingUnpublishedPost_postNotDisplayed(){
        // arrange
        Date lastUpdated = post.getLastUpdated();
        String author = post.getAuthor().getUsername();
        String expectedOutput = "";
        // act
        post.displayComponent();
        // assert
        assertEquals(expectedOutput, outContent.toString());
    }

    @Test
    public void publishPost_publishingExistingPost_postPublished(){
        // arrange
        boolean expectedValue = true;
        // act
        post.publishPost();
        // assert
        assertEquals(expectedValue, post.isPublished());
    }

    @Test
    public void registerTopic_registerPostToExistingTopic_postRegisteredToTopic(){
        // arrange
        Topic newTopic = new Topic("AngularJS", "Cachy content");
        boolean expectedValue = true;
        // act
        post.registerToTopic(newTopic);
        boolean actualValue = post.getArticleTopics().contains(newTopic);
        // assert
        assertEquals(expectedValue, actualValue);
    }




}