/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package genericmethodtest;

import java.io.PrintStream;

/**
 *
 * @author Matej
 */
public class GenericMethodTest {
    
    public static void printArray(Object[] array){
        printArray(array, System.out);
    }
    
    public static void printArray(Object[] array, PrintStream printTo){        
        printArray(array, printTo, ",");
    }
    
    public static void printArray(Object[] array, PrintStream printTo, String elementSeparator){
        printArray(array, printTo, elementSeparator, "\n");
    }
    
    public static void printArray(Object[] array, PrintStream printTo, String elementSeparator, String lineSeparator){
        boolean firstElement = true;
        for(Object o : array){
            if(firstElement)
                printTo.print(o.toString());
            else{
                printTo.print(elementSeparator);
                printTo.print(o.toString());
            }                
            firstElement = false;            
        }   
        printTo.print(lineSeparator);        
    }
}
