package cz.cvut.fel.omo.server;

import cz.cvut.fel.omo.Observable;
import cz.cvut.fel.omo.Observer;

import javax.money.MonetaryAmount;
import java.util.ArrayList;
import java.util.List;

public abstract class CryptoCurrencyController implements Observable {

    protected List<Observer> observers = new ArrayList<Observer>();
    protected CryptoCurrency currency;
    int PERCENTAGE = 100;

    public void changePrice(int fluctuation) {
        MonetaryAmount currentPrice = currency.getPrice();
        MonetaryAmount change = currentPrice.multiply(fluctuation).divide(PERCENTAGE);
        currency.setPrice(currentPrice.add(change));
        currency.printMessage();
        notifyAllObservers();
    }

    public void notifyAllObservers(){
        for(Observer observer: observers)
            observer.update();
    }


    public void attach(Observer observer){
        if (!observers.contains(observer))
            observers.add(observer);
    }

    public void detach(Observer observer){
        observers.remove(observer);
    }

    public MonetaryAmount getState(){
        return currency.getPrice();
    }
}
