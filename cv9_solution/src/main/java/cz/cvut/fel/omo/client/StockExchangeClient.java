package cz.cvut.fel.omo.client;

import cz.cvut.fel.omo.Observer;
import cz.cvut.fel.omo.server.StockExchangeServer;

public class StockExchangeClient {

    private StockExchangeServer stockExchangeServer;
    private String name;
    private Observer bitcoinClient;
    private Observer litecoinClient;

    public StockExchangeClient(StockExchangeServer stockExchange, String name){
        this.stockExchangeServer = stockExchange;
        this.name = name;
    }

    public void subscribeToBitcoinChannel(){
        if (bitcoinClient == null )
            bitcoinClient = new BitcoinClient(this);
        stockExchangeServer.subscribeToBitcoinUpdates(bitcoinClient);
    }

    public void subscribeToLitecoinChannel(){
        if (litecoinClient == null )
            litecoinClient = new LitecoinClient(this);
        stockExchangeServer.subscribeToLitecoinUpdates(litecoinClient);
    }

    public void subscribeToAllChannels(){
        subscribeToBitcoinChannel();
        subscribeToLitecoinChannel();
    }

    public void unsubscribeFromBitcoinChannel(){
        if (bitcoinClient != null)
            stockExchangeServer.unsubscribeFromBitcoinChannel(bitcoinClient);
    }

    public void unsubscribeFromLitecoinChannel(){
        if (litecoinClient != null)
            stockExchangeServer.unsubscribeFromLitecoinChannel(litecoinClient);
    }

    public void unsubscribeFromAllChannels(){
        unsubscribeFromBitcoinChannel();
        unsubscribeFromLitecoinChannel();
    }

    public String getName(){
        return name;
    }

    public StockExchangeServer getServer(){
        return stockExchangeServer;
    }
}
