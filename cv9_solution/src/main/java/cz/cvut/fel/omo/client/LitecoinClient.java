package cz.cvut.fel.omo.client;

import cz.cvut.fel.omo.Observer;

public class LitecoinClient implements Observer {

    StockExchangeClient context;

    public LitecoinClient(StockExchangeClient context){
        this.context = context;
    }

    public void update(){
        System.out.println(context.getName() + " notified about change of Litecoin to " + context.getServer().getLitecoinState());
    }
}
