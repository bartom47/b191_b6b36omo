package cz.cvut.fel.omo.cv8;

public class PrintListExpressionVisitor implements ListExpressionVisitor {

    @Override
    public void visitIntList(IntList v) {
        System.out.print(v.list);
    }

    @Override
    public void visitRemove(Remove r) {
        System.out.print("R(");
        r.sub.accept(this);
        System.out.print("," + r.element);
        System.out.println(")");
    }

    @Override
    public void visitUnique(Unique u) {
        System.out.print("U(");
        u.sub.accept(this);
        System.out.println(")");
    }

    @Override
    public void visitConcatenate(Concatenate c) {
        System.out.print("C(");
        c.left.accept(this);
        c.right.accept(this);
    }

    @Override
    public void visitVarList(VarList v) {
        System.out.println(v.name);
    }
    
    
}