package cz.cvut.fel.omo.cv8;

import com.google.common.collect.ImmutableList;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

public class Unique implements ListExpression{
    protected final ListExpression sub;

    public Unique(ListExpression sub) {
        this.sub = sub;
    }

    @Override
    public ImmutableList<Integer> evaluate(Context c) {
        List<Integer> l = new ArrayList<>(sub.evaluate(c));
        HashSet<Integer> set = new HashSet();
        l.forEach((i) -> {
            set.add(i);
        });        
        return ImmutableList.copyOf(set);
    }

    @Override
    public void accept(ListExpressionVisitor v) {
        v.visitUnique(this);
    }
    
    
    
}