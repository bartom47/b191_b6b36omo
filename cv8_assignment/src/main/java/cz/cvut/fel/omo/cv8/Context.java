package cz.cvut.fel.omo.cv8;

import com.google.common.collect.ImmutableList;
import java.util.HashMap;
import java.util.Map;

public class Context {
    
    Map<String, ImmutableList> map= new HashMap();    
    
    public void put(String key, ImmutableList value){
        map.put(key, value);
    }
    
    public ImmutableList get(String key){
        return map.get(key);
    }
    
}
