package cz.cvut.fel.omo.cv8;

public interface ListExpressionVisitor {
    void visitIntList(IntList v);

    // TODO: more visit methods

    public void visitRemove(Remove r);

    public void visitUnique(Unique u);

    public void visitConcatenate(Concatenate c);

    public void visitVarList(VarList v);
}
