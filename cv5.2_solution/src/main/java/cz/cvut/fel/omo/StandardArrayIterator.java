package cz.cvut.fel.omo;

import java.util.NoSuchElementException;

public class StandardArrayIterator implements Iterator {

    int [] array;
    int currentIndex = 0;

    public StandardArrayIterator(int [] array){
        this.array = array;
    }

    public int currentItem(){
        if (isEmpty())
            throw new NoSuchElementException();
        return array[currentIndex];
    }

    public int next(){
        if (currentIndex+1 < array.length)
            return array[++currentIndex];
        throw new NoSuchElementException();
    }

    public boolean isDone(){
        return currentIndex == array.length - 1;
    }

    public int first(){
        currentIndex = 0;
        return array[currentIndex];
    }

    private boolean isEmpty(){
        return array.length == 0;
    }
}
