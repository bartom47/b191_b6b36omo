package cz.cvut.fel.omo.cv6.state;


public class Go extends State{

    public Go(Context context) {
        super(context);
        this.color = Color.GREEN;
        this.period = LightPeriod.GREEN_LIGHT_PERIOD.getValue();
    }
    
    @Override
    protected void changeToNextState() {
        context.setState(new Attention(context));
    }
    
}

