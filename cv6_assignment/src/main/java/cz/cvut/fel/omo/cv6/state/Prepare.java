package cz.cvut.fel.omo.cv6.state;

public class Prepare extends State{

    public Prepare(Context context) {
        super(context);
        this.color = Color.ORANGE;
        this.period = LightPeriod.ORANGE_LIGHT_PERIOD.getValue();
    }
        
    
    @Override
    protected void changeToNextState() {
        context.setState(new Go(context));
    }
    
}
