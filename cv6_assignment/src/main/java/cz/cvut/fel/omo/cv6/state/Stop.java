package cz.cvut.fel.omo.cv6.state;

public class Stop extends State{

    public Stop(Context context) {
        super(context);
        this.color = Color.RED;
        this.period = LightPeriod.RED_LIGHT_PERIOD.getValue();
    }       
    
    @Override
    protected void changeToNextState() {
        context.setState(new Prepare(context));
    }    
}

